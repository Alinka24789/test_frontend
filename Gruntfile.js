// Gruntfile.js
module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        concat: {
            dist: {
                src: [
                    'assets/js/libs/*.js', // Все JS в папке libs
                    'assets/js/main.js'  // Конкретный файл
                ],
                dest: 'assets/js/production.js',
            }
        },
        uglify: {
            build: {
                src: 'assets/js/production.js',
                dest: 'assets/js/production.min.js'
            }
        },
        // Watch task config
        watch: {
            sass: {
                files: "assets/scss/*.scss",
                tasks: ['sass:dev', 'cssmin']
            },
            scripts: {
                files: ['assets/js/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    spawn: false,
                },
            }
        },
        // SASS task config
        sass: {
            dev: {
                files: {
                    // destination         // source file
                    "assets/css/styles.css": "assets/scss/styles.scss"
                }
            }
        },
        cssmin: {
            target: {
                files: [{
                        expand: true,
                        cwd: 'assets/css',
                        src: ['*.css', '!*.min.css'],
                        dest: 'assets/css',
                        ext: '.min.css'
                    }]
            }
        },
        bower_concat: {
            all: {
                dest:'assets/js/libs/libs.js',
                cssDest: 'assets/css/bootstrap.css',
            }
        },
        // inside Gruntfile.js
        // Using the BrowserSync Server for your static .html files.
        browserSync: {
            default_options: {
                bsFiles: {
                    src: [
                        "assets/css/*.css",
                        "*.html"
                    ]
                },
                options: {
                    proxy: "http://localhost:8383/test/index.html",
                    ghostmode: false,
                    port: 8080,
                    watchTask: true 
                }
            }
        }
    });

    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('buildbower', [
        'bower_concat',
    ]);
}
