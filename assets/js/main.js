/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function(){
   
    resize_search_field();
    
    get_categories();
    
    $(window).resize(function(){
        resize_search_field();
    });
   
});


function resize_search_field() {
    
    var blocks = $('[data-id="m-block"]');
    
    var total_width = $('.app-container').outerWidth();
    
    var sum_width = 0;
    
    blocks.each(function(key, val) {
        sum_width += $(val).outerWidth(true);
    });
    
    var result = total_width - sum_width - 30;
    
    $('#search-form').width(result);
    
}

function get_categories() {
    
    $.ajax({
        url: 'https://anypoint.mulesoft.com/apiplatform/proxy/http://dev.denteez.com/api/v2/services/categories',
        method: 'get',
        dataType: 'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', '29edc3a7914938252d1eadb273818475b973192f');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            
            $('#messageModal').find('.modal-body p').html('<strong> ' + jqXHR.status + ' ' + errorThrown + '!</strong> ' + jqXHR.responseJSON.error.description);

            $('#messageModal').modal('show');
        },
        success: function(response) {
            
            if(response.success == true) {
                
                var data = response.data;
                
                var template = $('#category-tmpl').html();
                
                $.template( "categoryTemplate", template );
                
                $('#categories-list').empty();
                
                var dataByRow = []
                
                var index = 0, row_index = 0;
                
                for(var key in data) {

                    if (index >= 0 && index <= 3) {

                        dataByRow[index] = data[key];

                        index++;

                    }

                    if (index == 4) {
                        
                        row_index++;
                        
                        $('#categories-list').append('<div class="categories-row categories-row-'+row_index+'"></div>');
                        
                        $.tmpl("categoryTemplate", dataByRow)
                                .appendTo('.categories-row-'+row_index);
                        
                        index = 0;
                    }

                }
                
            } 
             
        }
    })
}